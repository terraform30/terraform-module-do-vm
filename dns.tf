data "cloudflare_zones" "zone" {
  filter {
    name = var.domain
  }
}

resource "cloudflare_record" "a_record" {
  count   = var.external ? 1 : 0
  zone_id = lookup(data.cloudflare_zones.zone.zones[0], "id")
  name    = var.a_record != "" ? "${var.a_record == "@" ? var.domain : var.a_record}" : var.hostname
  value   = digitalocean_droplet.droplet.ipv4_address
  type    = "A"
  proxied = "true"
}

resource "digitalocean_record" "a_record" {
  name   = var.a_record != "" ? var.a_record : var.hostname
  type   = "A"
  domain = var.domain
  value  = var.external ? digitalocean_droplet.droplet.ipv4_address_private : digitalocean_droplet.droplet.ipv4_address
}

resource "digitalocean_record" "cname_record" {
  name   = var.alias
  type   = "CNAME"
  domain = var.domain
  value  = var.a_record != "" ? var.a_record : var.hostname
}
