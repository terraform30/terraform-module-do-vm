variable "hostname" {
  type = string
}

variable "region" {
  type    = string
  default = "lon1"
}

variable "size" {
  type    = string
  default = "s-1vcpu-1gb"
}

variable "vpc_uuid" {
  type    = string
  default = null
}

variable "private_networking" {
  type    = bool
  default = false
}

variable "tags" {
  type    = list(string)
  default = null
}

variable "ssh_keys" {
  type    = list(string)
  default = null
}

variable "domain" {
  type    = string
  default = null
}

variable "external" {
  type    = bool
  default = false
}

variable "alias" {
  type = string
}

variable "image_slug" {
  type    = string
  default = "ubuntu-18-04-x64"
}

variable "image_id" {
  type    = number
  default = null
}

variable "a_record" {
  type    = string
  default = null
}

variable "backups" {
  type    = bool
  default = null
}
