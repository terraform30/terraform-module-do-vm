resource "digitalocean_droplet" "droplet" {
  image              = var.image_id != null ? var.image_id : data.digitalocean_image.image.image
  name               = var.hostname
  region             = var.region
  size               = var.size
  vpc_uuid           = var.vpc_uuid
  private_networking = var.private_networking
  tags               = var.tags
  ssh_keys           = var.ssh_keys
  backups            = var.backups
}
