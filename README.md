# Terraform Module DO VM

- [1 Purpose](#1-purpose)
- [2 Terraform Config](#2-terraform-config)
  - [2.1 Providers](#21-providers)
    - [2.1.1 Cloudflare](#211-cloudflare)
    - [2.1.2 DigitalOcean](#212-digitalocean)
  - [2.2 Variables](#22-variables)
- [3 Example usage](#3-example-usage)
- [4 Versioning](#4-versioning)
- [5 Changelog](#5-changelog)

## 1 Purpose

This module's purpose is to deploy VMs (droplets) on digital ocean, and configure DNS and VPC connections. 

## 2 Terraform Config

### 2.1 Providers

This module uses two providers:
* `terraform-providers/digitalocean`
* `terraform-providers/cloudflare`

#### 2.1.1 Cloudflare

This module is used to set externally-reachable DNS addresses. 

#### 2.1.2 DigitalOcean

This module is used to create the VM (droplet) and internal DNS records.

### 2.2 Variables

| Name                 | Type           | Required | Default              | Description                                     |
| -------------------- | -------------- | -------- | -------------------- | ----------------------------------------------- |
| `hostname`           | `string`       | Yes      | N/A                  | Hostname of the VM                              |
| `region`             | `string`       | No       | `"lon1"`             | Region to provision in                          |
| `size`               | `string`       | No       | `"s-1vcpu-1gb"`      | Size of the VM                                  |
| `vpc_uuid`           | `string`       | No       | `null`               | UUID of the VPC to connect to (optional)        |
| `private_networking` | `bool`         | No       | `false`              | Whether to enable or disable private networking |
| `tags`               | `list(string)` | No       | `null`               | Tags for the VM                                 |
| `ssh_keys`           | `list(string)` | No       | `null`               | SSH Keys for the root user                      |
| `domain`             | `string`       | No       | `null`               | Domain to use for DNS records                   |
| `external`           | `bool`         | No       | `false`              | Use external DNS records (Cloudflare)           |
| `alias`              | `string`       | Yes      | N/A                  | Alias (CNAME) record                            |
| `image_slug`         | `string`       | No       | `"ubuntu-18-04-x64"` | Image to provision with                         |
| `image_id`           | `number`       | No       | `null`               | ID of the image (if there is no slug)           |
| `a_record`           | `string`       | No       | `null`               | Name of A record                                |
| `backups`            | `bool`         | No       | `null`               | Whether to enable backups                       |

## 3 Example usage

In its most basic form, this module can be used as follows:

```terraform
module "my-module" {
    source   = ...
    hostname = "my-hostname"
    alias    = "my-alias
}
```
This will provision a VM with the hostname `"my-hostname"` and create a CNAME record in DigitalOcean of `"my-alias"`.

## 4 Versioning

This module uses the versioning standard describe by semver 2.0.0.

## 5 Changelog

You can view changes and version history in the [changelog](CHANGELOG.md).